import React from "react";
import { Routes, Route } from "react-router-dom";
import Login from "@/pages/Login/Login";
// SuperAdmin Routes 
import SuperAdmin from "@/pages/SuperAdmin/SuperAdmin";
import SAUsers from "@/pages/SuperAdmin/Users/Users";
import projectsData from "@/pages/SuperAdmin/Projects/projectsData";
import SAProjectList from "@/pages/SuperAdmin/Projects/Projects";
import SAProjectDetails from "@/pages/SuperAdmin/Projects/ProjectDetails";

const App = () => {
  return (
    <div className="overflow-hidden">
      <Routes>
        <Route path="/" exact element={<Login />} />
        <Route path="superadmin" exact element={<SuperAdmin />} />
        <Route path="/superadmin/users" exact element={<SAUsers />} />
        <Route path="superadmin/projects" element={<SAProjectList projects={projectsData.projects} />} />
        <Route path="superadmin/projects/:projectId" element={<SAProjectDetails projects={projectsData.projects} />} />
      </Routes>
    </div>
  );
};

export default App;
