// NavigationBar.js
import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { BsIncognito } from "react-icons/bs";

const SideNav = () => {
    const { pathname } = useLocation();

    return (
        <div>
            <div className="shadow-md">
                <div className='p-2 font-title shadow-md text-xl flex items-center bg-white z-10 fixed w-full'>
                    <div className='text-2xl bg-white rounded-full p-2'><BsIncognito /></div>
                    <div className=''>TechOps Solutions</div>
                </div>
            </div>
            <div className="h-[100vh] shadow-md border-r w-[18%] fixed bg-white top-0 pt-20 z-5">
                <ul className="ml-8 pt-8 font-title" id="saNavActive">
                    <Link to="/superadmin">
                        <li className={pathname === '/superadmin' ? 'active' : ''}>Home</li>
                    </Link>
                    <Link to="/superadmin/users">
                        <li className={pathname === '/superadmin/users' ? 'active' : ''}>Users</li>
                    </Link>
                    <Link to="/superadmin/projects">
                        <li className={pathname === '/superadmin/projects' ? 'active' : ''}>Projects</li>
                    </Link>
                </ul>
            </div>
        </ div>
    );
};

export default SideNav;
