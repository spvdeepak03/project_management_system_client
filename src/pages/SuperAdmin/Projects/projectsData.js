
const projectsData = {
  projects: [
    {
      id: 1,
      name: "project 1",
      description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, porro magni quo aut suscipit dicta eveniet voluptatibus error aliquam eligendi commodi, culpa fugiat iure dolor. Repudiandae ipsa, fugiat",
      startDate: "2024-05-08",
      endDate: "2024-05-28",
      status: "active",
      projectManagers: "manager1",
      projectDevelopers: ["dev1", "dev2", "dev3"],
      workItems: [
        {
          title: "Item 1",
          description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, porro magni quo aut suscipit dicta eveniet voluptatibus error aliquam eligendi commodi, culpa fugiat iure dolor. Repudiandae ipsa, fugiat",
          devassignees: ["dev1", "dev2"],
          priority: "high",
          status: "new",
          comments: ["Started Today", "curious"],
          startDate: "2024-05-08",
          endDate: "2024-05-28",
        },
        {
          title: "Item 2",
          description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, porro magni quo aut suscipit dicta eveniet voluptatibus error aliquam eligendi commodi, culpa fugiat iure dolor. Repudiandae ipsa, fugiat",
          devassignees: ["dev5", "dev12", "dev3"],
          priority: "low",
          status: "active",
          // comments: ["Again?", "Created Local Repo"],
          startDate: "2024-05-08",
          endDate: "2024-06-20",
        }
      ]
    },
    {
      id: 2,
      name: "project2",
      description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, porro magni quo aut suscipit dicta eveniet voluptatibus error aliquam eligendi commodi, culpa fugiat iure dolor. Repudiandae ipsa, fugiat",
      startDate: "2024-05-08",
      endDate: "2024-05-28",
      status: "active",
      projectManagers: ["manager 1", "manager 2"],
      projectDevelopers: ["dev1", "dev2"],
      workItems: [
        {
          title: "Item 1",
          description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, porro magni quo aut suscipit dicta eveniet voluptatibus error aliquam eligendi commodi, culpa fugiat iure dolor. Repudiandae ipsa, fugiat",
          devassignees: ["dev1", "dev2"],
          pmassignees: ["manager 1", "manager 2"],
          priority: "high",
          status: "new",
          // comments: ["hi", "bye"], 
          startDate: "2024-05-08",
          endDate: "2024-05-28",
        }
      ]
    },
  ],
};

export default projectsData;
