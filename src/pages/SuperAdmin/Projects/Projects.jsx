import NavBar from "@/pages/SuperAdmin/components/Nav/SideNav";
import React from 'react';
import { Link } from 'react-router-dom'; // Import Link from react-router-dom
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
} from "@/components/ui/dialog";
import { MdOutlineEmail } from "react-icons/md";
import { FaRegUserCircle } from "react-icons/fa";

const ProjectList = ({ projects }) => {
    return (
        <div className="w-full font-primary bg-[#f6f9ff] min-h-screen">
            <NavBar />
            <div className="pl-80 pt-32 font-title">
                <div>
                    <h2>Project List</h2>
                    <div className="flex">
                        {projects.map(project => (
                            <div key={project.id}>
                                <div className="m-5 border p-5 min-w-80 bg-white rounded-md">
                                    <Link to={`/superadmin/projects/${project.id}`}>
                                        <div className="">
                                            <div className="text-2xl border-b pb-2"><span className="text-gray-400"># </span>{project.id}</div>
                                            <div className="text-xl pt-2 capitalize">{project.name}</div>
                                            <div className="pt-3"><span className="text-gray-500">Due: </span>{project.endDate}</div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        ))}

                    </div>
                </div>
                <div className="absolute top-28 right-0 mr-10 overflow-y-scroll">
                    <Dialog className='overflow-y-scroll'>
                        <DialogTrigger>
                            <div>
                                <button className="bg-[#277bf9] hover:bg-[#0d6efd] py-2 px-3 text-white rounded-md">+ Create New Project</button>
                            </div>
                        </DialogTrigger>
                        <DialogContent className="overflow-y-scroll">
                            <DialogHeader>
                                <DialogTitle>Create New Project</DialogTitle>
                                <DialogDescription>
                                    <form className='w-full flex flex-col items-center justify-center mt-5 font-primary overflow-y-scroll' method='post'>
                                        <div className='w-full relative'>
                                            <label for='Pname' className='mb-2 block text-gray-600'>Project Title</label>
                                            <div className='flex flex-row-reverse'>
                                                <input type='name' id='Pname' placeholder='' name='Pname' required className='text-black block border-t border-b border-r border-gray-300 focus:border-blue-500 focus:border-2 outline-none px-2 py-[9px] w-[100%] rounded-e-md   ' />
                                                <div className='block bottom-2 text-xl px-3 border-r border-gray-300 py-3 rounded-s-md text-gray-700 bg-gray-200' >
                                                    <MdOutlineEmail />
                                                </div>
                                            </div>
                                        </div>
                                        <div className='w-full relative mt-5'>
                                            <label for='Pdes' className='mb-2 block text-gray-600'>Description</label>
                                            <div className='flex flex-row-reverse'>
                                                <textarea id='Pdes' placeholder='' name='Pdes' required className='text-black block border-t border-b border-r border-gray-300 focus:border-blue-500 focus:border-2 outline-none px-2 py-[9px] w-[100%] rounded-e-md'> </textarea>
                                                <div className='block bottom-2 text-xl px-3 border-r border-gray-300 py-3 rounded-s-md text-gray-700 bg-gray-200' >
                                                    <MdOutlineEmail />
                                                </div>
                                            </div>
                                        </div>
                                        <div className='w-full relative mt-5'>
                                            <label for='PSdate' className='mb-2 block text-gray-600'>Start Date</label>
                                            <div className='flex flex-row-reverse'>
                                                <input type="date" id='PSdate' placeholder='' name='PSdate' required className='text-black block border-t border-b border-r border-gray-300 focus:border-blue-500 focus:border-2 outline-none px-2 py-[9px] w-[100%] rounded-e-md' />
                                                <div className='block bottom-2 text-xl px-3 border-r border-gray-300 py-3 rounded-s-md text-gray-700 bg-gray-200' >
                                                    <MdOutlineEmail />
                                                </div>
                                            </div>
                                        </div>
                                        <div className='w-full relative mt-5'>
                                            <label for='PEdate' className='mb-2 block text-gray-600'>End Date</label>
                                            <div className='flex flex-row-reverse'>
                                                <input type="date" id='PEdate' placeholder='' name='PEdate' required className='text-black block border-t border-b border-r border-gray-300 focus:border-blue-500 focus:border-2 outline-none px-2 py-[9px] w-[100%] rounded-e-md' />
                                                <div className='block bottom-2 text-xl px-3 border-r border-gray-300 py-3 rounded-s-md text-gray-700 bg-gray-200' >
                                                    <MdOutlineEmail />
                                                </div>
                                            </div>
                                        </div>
                                        <div className='w-full relative mt-5'>
                                            <label for='Pmanager' className='mb-2 block text-gray-600'>Select Project Manager</label>
                                            <div className='flex flex-row-reverse'>
                                                <select id='Pmanager' name='Pmanager' required className='text-black block border-t border-b border-r border-gray-300 focus:border-blue-500 focus:border-2 outline-none px-2 py-[9px] w-[100%] rounded-e-md' >
                                                    <option value="">- Select -</option>
                                                    <option value="Manager1">Manager1</option>
                                                    <option value="Manager2">Manager2</option>
                                                    <option value="Manager3">Manager3</option>
                                                    <option value="Manager4">Manager4</option>
                                                </select>
                                                <div className='block bottom-2 text-xl px-3 border-r border-gray-300 py-3 rounded-s-md text-gray-700 bg-gray-200' >
                                                    <FaRegUserCircle />
                                                </div>
                                            </div>
                                        </div>
                                        <div className='mt-5 w-full'>
                                            <button type='submit' className='font-title bg-[#0c5ae9] hover:bg-[#114abcee] w-full text-white rounded-md mt-2 py-2'>Create Project</button>
                                        </div>
                                    </form>
                                </DialogDescription>
                            </DialogHeader>
                        </DialogContent>
                    </Dialog>
                </div>
            </div>
        </div>

    );
};

export default ProjectList;