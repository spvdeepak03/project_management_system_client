import React from 'react';
import NavBar from "@/pages/SuperAdmin/components/Nav/SideNav";
import { useParams } from 'react-router-dom';
import { FaUserTie, FaCode } from "react-icons/fa6";
import {
    Accordion,
    AccordionContent,
    AccordionItem,
    AccordionTrigger,
} from "@/components/ui/accordion";
import { LuPenLine } from "react-icons/lu";
import { IoCheckmarkDone } from "react-icons/io5";
import { RiDeleteBin6Line } from "react-icons/ri";

const ProjectDetails = ({ projects }) => {
    const { projectId } = useParams(); // Get projectId from URL params

    // Find the project with the matching id
    const project = projects.find(project => project.id === parseInt(projectId));

    // If project is not found, display a message
    if (!project) {
        return (
            <div>
                <NavBar />
                <div className='pl-80 pt-32 font-title text-3xl'>OOPS! project not found</div>
            </div>
        );
    }

    return (
        <div className='w-full font-secondary bg-[#f6f9ff] mb-10 h-full'>
            <NavBar />
            <div className='pl-80 pt-32 font-secondary'>
                <div className='bg-white p-5 w-[90%] shadow-md rounded-md mb-5'>
                    <h2 className='font-title text-2xl border-b pb-2'>
                        <span className='text-gray-400'>#</span><span className='ml-1'>{project.id} /</span>
                        <span className='ml-3 uppercase'>{project.name}</span>
                    </h2>
                    <p className='mt-3 mb-3'>{project.description}</p>
                    <div className='tracking-wide mb-2 px-4'>Start Date: {project.startDate}</div>
                    <div className='flex'>
                        <div className='tracking-wider bg-orange-300 py-2 px-4 rounded-full'>Due Date: {project.endDate}</div>
                        <div className='ml-5 mr-10 py-2 px-4 bg-green-400 rounded-full capitalize font-title'>{project.status}</div>
                        <div className='flex items-center ml-auto mr-10 py-2 px-4 bg-blue-500 text-white rounded-lg cursor-pointer capitalize font-title'>
                            <div><LuPenLine /> </div>
                            <div className='ml-2'>Edit</div>
                        </div>
                    </div>
                </div>
                <div className='flex flex-row-reverse max-w-[90%]'>
                    <div className='bg-white p-5 w-[30%] shadow-md rounded-md ml-3 h-max'>
                        <div className='font-title mb-2 pb-2 border-b text-lg'>Project Managers</div>
                        <div className='text-lg ml-2'>{Array.isArray(project.projectManagers) ? project.projectManagers.map(manager =>
                            <div className='ml-5 flex items-center mt-3'>
                                <div className='text-blue-500'><FaUserTie /></div>
                                <div className='ml-2 text-lg' key={manager}>{manager}</div></div>) : project.projectManagers}
                        </div>
                        <div className='font-title mb-2 pb-2 border-b mt-10 text-lg'>Project Developers</div>
                        <div className='text-lg ml-2'>{Array.isArray(project.projectDevelopers) ? project.projectDevelopers.map(developer =>
                            <div className='ml-5 flex items-center mt-3'>
                                <div className='text-green-500'><FaCode /></div>
                                <div className='ml-2 text-lg' key={developer}>{developer}</div></div>) : project.projectDevelopers}
                        </div>
                    </div>
                    <div className='bg-white p-5 shadow-lg rounded-md mb-5 w-[70%]'>
                        <h3 className='font-title mb-2 pb-2 border-b text-lg'>Work Items</h3>
                        <div>
                            <Accordion type="single" collapsible className="w-[100%]">
                                {project.workItems.map((item, index) => (
                                    <AccordionItem value={item.title}>
                                        <div className='bg-white p-5 w-[100%] shadow-lg border rounded-md mt-5' key={index}>
                                            <AccordionTrigger>
                                                <div className='font-title text-xl tracking-wider uppercase'>
                                                    <span className='text-gray-400'>#</span>
                                                    <span className='ml-1'>{item.title}</span>
                                                </div>
                                            </AccordionTrigger>
                                            <AccordionContent>
                                                <div className=' mt-1 pt-3 border-t '>{item.description}</div>
                                                <div className='flex mt-4'>
                                                    <div className='mr-8 py-2 px-4 bg-purple-400 rounded-full capitalize'>Priority: {item.priority}</div>
                                                    <div className='py-2 px-4 bg-green-400 rounded-full capitalize'>Status: {item.status}</div>
                                                </div>
                                                <div className='tracking-wide mb-2 px-4 mt-5'>Start Date: {item.startDate}</div>
                                                <div className='tracking-wider bg-orange-300 py-2 px-4 rounded-full w-max'>Due Date: {item.endDate}</div>
                                                <div className='mt-5 bg-slate-100 p-5'>
                                                    <div className='font-title border-b pb-2 mb-3'>Assigned to</div>
                                                    <div className='flex'>
                                                        {Array.isArray(item.devassignees) ? item.devassignees.map(assignee => (
                                                            <div className='px-4 py-2 bg-blue-500 rounded-lg mr-3 text-white' key={assignee}>{assignee}</div>
                                                        )) : item.devassignees}
                                                    </div>
                                                </div>
                                            </AccordionContent>
                                        </div>
                                    </AccordionItem>
                                ))}
                            </Accordion>
                        </div>
                    </div>
                </div>
                <div className='flex'>
                    <div className='flex items-center mr-5 py-2 px-4 bg-blue-500 text-white rounded-md cursor-pointer capitalize font-title'>
                        <div><IoCheckmarkDone /> </div>
                        <div className='ml-2'>Mark as Completed</div>
                    </div>
                    <div className='flex items-center mr-5 py-1 px-4 bg-red-500 text-white rounded-md cursor-pointer capitalize font-title'>
                        <div><RiDeleteBin6Line /> </div>
                        <div className='ml-2'>Delete Project</div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProjectDetails;
