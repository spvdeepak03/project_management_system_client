import React, { useState } from "react";
import userData from './usersData';
import NavBar from "@/pages/SuperAdmin/components/Nav/SideNav";
import { MdOutlineEmail } from "react-icons/md";
import { FaRegUserCircle } from "react-icons/fa";
import {
    Table,
    TableBody,
    TableCaption,
    TableCell,
    TableHead,
    TableHeader,
    TableRow,
} from "@/components/ui/table";
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
} from "@/components/ui/dialog";
import { LuPenLine } from "react-icons/lu";
import { RiDeleteBin6Line } from "react-icons/ri";
import { GoDot, GoDotFill } from "react-icons/go";

const Users = () => {
    const [users, setUsers] = useState([]);
    const [activeButton, setActiveButton] = useState(null);

    const handleCategoryClick = (category) => {
        setUsers(userData[category]);
        setActiveButton(category);
    };

    return (
        <div className="w-full font-primary bg-[#f6f9ff] h-screen">
            <NavBar />
            <div className="pl-80 pt-32 font-title">
                <div className="flex">
                    <div className="bg-white w-max rounded-md" id="userRoleNavButton">
                        <button
                            onClick={() => handleCategoryClick('admins')}
                            className={activeButton === 'admins' ? 'active' : ''}
                        >
                            Admins
                        </button>
                        <button
                            onClick={() => handleCategoryClick('projectManagers')}
                            className={activeButton === 'projectManagers' ? 'active' : ''}
                        >
                            Project Managers
                        </button>
                        <button
                            onClick={() => handleCategoryClick('developers')}
                            className={activeButton === 'developers' ? 'active' : ''}
                        >
                            Developers
                        </button>
                    </div>
                    <div className="ml-auto mr-10">
                        <Dialog>
                            <DialogTrigger>
                                <div>
                                    <button className="bg-[#277bf9] hover:bg-[#0d6efd] py-2 px-3 text-white rounded-md">+ Create New User</button>
                                </div>
                            </DialogTrigger>
                            <DialogContent>
                                <DialogHeader>
                                    <DialogTitle>Create New User</DialogTitle>
                                    <DialogDescription>
                                        <form className='w-full flex flex-col items-center justify-center mt-5 font-primary' method='post'>
                                            <div className='w-full relative'>
                                                <label for='email' className='mb-2 block text-gray-600'>New User's Email Id</label>
                                                <div className='flex flex-row-reverse'>
                                                    <input type='email' id='email' placeholder='' name='email' required className='text-black block border-t border-b border-r border-gray-300 focus:border-blue-500 focus:border-2 outline-none px-2 py-[9px] w-[100%] rounded-e-md   ' />
                                                    <div className='block bottom-2 text-xl px-3 border-r border-gray-300 py-3 rounded-s-md text-gray-700 bg-gray-200' >
                                                        <MdOutlineEmail />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='w-full relative mt-5'>
                                                <label for='userrole' className='mb-2 block text-gray-600'>Select Role</label>
                                                <div className='flex flex-row-reverse'>
                                                    <select id='userrole' name='userrole' required className='text-black block border-t border-b border-r border-gray-300 focus:border-blue-500 focus:border-2 outline-none px-2 py-[9px] w-[100%] rounded-e-md' >
                                                        <option value="">- Select -</option>
                                                        <option value="superadmin">Super Admin</option>
                                                        <option value="admin">Admin</option>
                                                        <option value="pmanager">Project Manager</option>
                                                        <option value="developer">Developer</option>
                                                    </select>
                                                    <div className='block bottom-2 text-xl px-3 border-r border-gray-300 py-3 rounded-s-md text-gray-700 bg-gray-200' >
                                                        <FaRegUserCircle />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='mt-5 w-full'>
                                                <button type='submit' className='font-title bg-[#0c5ae9] hover:bg-[#114abcee] w-full text-white rounded-md mt-2 py-2'>Create User</button>
                                            </div>
                                        </form>
                                    </DialogDescription>
                                </DialogHeader>
                            </DialogContent>
                        </Dialog>
                    </div>
                </div>
                <div className="flex items-center justify-center flex-col">
                    <Table className="bg-white mt-10 w-[90%] rounded-md">
                        <TableCaption>Select a Role for Users List</TableCaption>
                        <TableHeader>
                            <TableRow>
                                <TableHead className="w-[100px]">UserName</TableHead>
                                <TableHead>Email Id</TableHead>
                                <TableHead>Status</TableHead>
                                <TableHead className="">Edit</TableHead>
                                <TableHead className="">Delete</TableHead>
                            </TableRow>
                        </TableHeader>
                        <TableBody>
                            {users.map((user, index) => (
                                <TableRow className="font-primary">
                                    <TableCell className="font-medium" key={index}>@{user.username}</TableCell>
                                    <TableCell>{user.email}</TableCell>
                                    <TableCell className="capitalize">
                                        <div className="flex items-center justify-center w-max">
                                            <div className={`${user.status === 'active' ? 'block' : 'hidden'} ${user.status === 'active' ? 'text-green-500' : ''}`}><GoDotFill className="mr-1" /></div>
                                            <div className={`${user.status === 'pending' ? 'block' : 'hidden'} ${user.status === 'pending' ? 'text-orange-500' : ''}`}><GoDot className="mr-1" /></div>
                                            <div className={user.status === 'active' ? 'text-green-500' : 'text-orange-500'}>{user.status}</div>
                                        </div>
                                    </TableCell>
                                    <TableCell className="text-right text-blue-500">
                                        <div className="flex items-center cursor-pointer justify-center w-max">
                                            <div><LuPenLine /></div>
                                            <div className="ml-2">Edit</div>
                                        </div>
                                    </TableCell>
                                    <TableCell className="text-right text-red-500">
                                        <div className="flex items-center cursor-pointer justify-center w-max">
                                            <div><RiDeleteBin6Line /></div>
                                            <div className="ml-2">Delete</div>
                                        </div>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </div>
            </div>
        </div>
    )
}

export default Users;