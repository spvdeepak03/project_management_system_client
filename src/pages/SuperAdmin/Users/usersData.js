const usersData = {
  admins: [
    { username: "admin1", email: "admin1@example.com", status: "active" },
    { username: "admin2", email: "admin2@example.com", status: "active" },
    { username: "admin3", email: "admin2@example.com", status: "active" },
    { username: "admin4", email: "admin2@example.com", status: "pending" },
    { username: "admin5", email: "admin2@example.com", status: "pending" },
  ],
  projectManagers: [
    { username: "pm1", email: "pm1@example.com", status: "active" },
    { username: "pm2", email: "pm2@example.com", status: "pending" }
  ],
  developers: [
    { username: "dev1", email: "dev1@example.com", status: "active" },
    { username: "dev2", email: "dev2@example.com", status: "pending" }
  ]
};

export default usersData;
