import React from 'react';
// import { MdOutlineEmail } from "react-icons/md"; 
import { BsKey } from "react-icons/bs";
import { FaRegUserCircle, FaAt } from "react-icons/fa";
import { BsIncognito } from "react-icons/bs";

const Login = () => {
    return (
        <div className='bg-[#f6f9ff] h-[100vh] w-full flex flex-col items-center justify-center font-primary' id='loginBg'>
            <div className='m-5 font-title text-2xl flex items-center justify-center'>
                <div className='text-3xl bg-white rounded-full p-2'><BsIncognito /></div>
                <div className='ml-2'>TechOps Solutions</div>
            </div>
            <div className='flex flex-col items-center justify-center shadow-xl p-6 bg-white w-[400px] rounded-md'>
                <div className='w-full'>
                    <form className='w-full flex flex-col items-center justify-center' method='post'>
                        <div className='w-full relative'>
                            <label for='username' className='mb-2 block text-gray-600'>Username</label>
                            <div className='flex flex-row-reverse'>
                                <input type='text' id='username' placeholder='' name='username' required className='block border-t border-b border-r border-gray-300 focus:border-blue-500 focus:border-2 outline-none px-2 py-[9px] w-[100%] rounded-e-md   ' />
                                <div className='block bottom-2 text-xl px-3 border-r border-gray-300 py-3 rounded-s-md text-gray-700 bg-gray-200' >
                                    <FaAt />
                                </div>
                            </div>
                        </div>
                        <div className='w-full relative mt-5'>
                            <label for='userrole' className='mb-2 block text-gray-600'>Select Role</label>
                            <div className='flex flex-row-reverse'>
                                <select id='userrole' name='userrole' required className='block border-t border-b border-r border-gray-300 focus:border-blue-500 focus:border-2 outline-none px-2 py-[9px] w-[100%] rounded-e-md' >
                                    <option value="">- Select -</option>
                                    <option value="superadmin">Super Admin</option>
                                    <option value="admin">Admin</option>
                                    <option value="pmanager">Project Manager</option>
                                    <option value="developer">Developer</option>
                                </select>
                                <div className='block bottom-2 text-xl px-3 border-r border-gray-300 py-3 rounded-s-md text-gray-700 bg-gray-200' >
                                    <FaRegUserCircle />
                                </div>
                            </div>
                        </div>
                        <div className='w-full relative mt-5'>
                            <label for='password' className='mb-2 block text-gray-600'>Password</label>
                            <div className='flex flex-row-reverse'>
                                <input type='password' id='password' placeholder='' name='password' required className='block border-t border-b border-r border-gray-300 focus:border-blue-500 focus:border-2 outline-none px-2 py-[9px] w-[100%] rounded-e-md   ' />
                                <div className='block bottom-2 text-xl px-3 border-r border-gray-300 py-3 rounded-s-md text-gray-700 bg-gray-200' >
                                    <BsKey />
                                </div>
                            </div>
                        </div>
                        <div className='flex ml-auto mt-2 text-sm italic underline text-[#0d6efd]'>
                            <a href=''>Forgot Password?</a>
                        </div>
                        <div className='mt-5 w-full'>
                            <button type='submit' className='font-title bg-[#0c5ae9] hover:bg-[#114abcee] w-full text-white rounded-md mt-2 py-2'>Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Login;
